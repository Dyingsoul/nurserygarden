<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<section class="main-body">
    <div class="container">
        <div class="row-fluid">

            <!--<div class="span8">-->

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'homeLink' => CHtml::link('Főoldal', Yii::app()->homeUrl),
                    'htmlOptions' => array('class' => 'breadcrumb')
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <!-- Include content pages -->
            <?php echo $content; ?>
        </div><!--/row-->
    </div>
</section>


<?php $this->endContent(); ?>