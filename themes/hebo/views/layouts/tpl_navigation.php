<section id="navigation-main">  
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="nav-collapse">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'htmlOptions' => array('class' => 'nav'),
                        'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
                        'itemCssClass' => 'item-test',
                        'encodeLabel' => false,
                        'items' => array(
                            array('label' => 'Betontípusok', 'url' => array('/recipe/index'), 'itemOptions' => array('class' => 'dropdown', 'tabindex' => "-1"),
                                'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => "dropdown"),
                                'items' => array(
                                    array('label' => 'Alapanyag Típusok', 'url' => array('/componentType/index')),
                                    array('label' => 'Konzisztencia', 'url' => array('/consistency/index')),
                                    array('label' => 'Környzeti Osztályok', 'url' => array('/environmentClass/index')),
                                    array('label' => 'Nyomószilárdság', 'url' => array('/compressiveStrength/index')),
                                    array('label' => 'Betontípusok', 'url' => array('/recipe/index')),
                                    array('label' => 'Szemszerkezet', 'url' => array('/grain/index')),
                                )),
                            array('label' => 'Járművek', 'url' => array('/vehicle/index'), 'itemOptions' => array('class' => 'dropdown', 'tabindex' => "-1"),
                                'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => "dropdown"),
                                'items' => array(
                                    array('label' => 'Gépkocsivezetők', 'url' => array('/driver/index')),
                                    array('label' => 'Jármű Típusok', 'url' => array('/vehicleType/index')),
                                    array('label' => 'Költség Típusok', 'url' => array('/expenseType/index')),
                                )),
                            array('label' => 'Megrendelők', 'url' => array('/customer/index')),
                            array('label' => 'Szállítólevelek', 'url' => array('/delivery/index')),
                            array('label' => 'Munkalapok', 'url' => array('/worksheet/index')),
                            array('label' => 'Felhasználók', 'url' => array('/user/index')),
                            array('label' => 'Lekérdezések', 'url' => array('/queries/index')),
                            array('label' => 'Bejelentkezés', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                            array('label' => 'Kijelentkezés (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section><!-- /#navigation-main -->