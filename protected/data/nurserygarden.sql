-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2015. Dec 20. 15:25
-- Kiszolgáló verziója: 10.0.17-MariaDB
-- PHP verzió: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `nurserygarden`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `date` int(10) UNSIGNED NOT NULL,
  `payment` enum('készpénz','átutalás','utánvét') NOT NULL,
  `delivery` enum('futárszolgálat','postai kézbesítés','személyes átvétel') NOT NULL,
  `comment` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `order`
--

INSERT INTO `order` (`id`, `user_id`, `date`, `payment`, `delivery`, `comment`) VALUES
(2, 4, 1450471136, 'készpénz', 'futárszolgálat', ''),
(3, 4, 1450471150, 'átutalás', 'személyes átvétel', ''),
(4, 4, 1450608549, 'készpénz', 'személyes átvétel', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `order_plant`
--

CREATE TABLE `order_plant` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `plant_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `order_plant`
--

INSERT INTO `order_plant` (`order_id`, `plant_id`, `quantity`) VALUES
(4, 1, 2),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `plant`
--

CREATE TABLE `plant` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `property` text,
  `requirement` text,
  `plantable_from` varchar(10) DEFAULT NULL,
  `plantable_to` varchar(10) DEFAULT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `plant`
--

INSERT INTO `plant` (`id`, `name`, `type_id`, `property`, `requirement`, `plantable_from`, `plantable_to`, `quantity`, `price`) VALUES
(1, 'teszt', 1, 'jellemzők', 'igények', '08.20', '10.30', 50, '2000.00'),
(2, 'eeee', 2, 'easdasd', 'asdasda', '10.02', '12.11', 30, '123546.00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `plant_type`
--

CREATE TABLE `plant_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `unit` enum('darab','tő','szál') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `plant_type`
--

INSERT INTO `plant_type` (`id`, `name`, `description`, `unit`) VALUES
(1, 'Fák', '', 'darab'),
(2, 'teszt', '', 'darab');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(64) NOT NULL,
  `group` enum('vásárló','dolgozó','vezető') NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `group`, `address`) VALUES
(1, 'Admin', 'info@nurserygarden.com', 'Admin', 'vezető', '1234 Bivajbasznád Tököli út 6'),
(2, 'test', 'test@test.hu', 'test', 'vásárló', 'test'),
(3, 'test2', 'asd@asd.hu', 'test2', 'dolgozó', 'test2'),
(4, 'Pisi Misi', 'pisi@misi.hu', 'misi', 'vásárló', 'istenhátamögött');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_user_index` (`user_id`);

--
-- A tábla indexei `order_plant`
--
ALTER TABLE `order_plant`
  ADD PRIMARY KEY (`order_id`,`plant_id`),
  ADD KEY `fk_order_plant_index` (`order_id`),
  ADD KEY `fk_plant_order_index` (`plant_id`);

--
-- A tábla indexei `plant`
--
ALTER TABLE `plant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_plant_type_index` (`type_id`);

--
-- A tábla indexei `plant_type`
--
ALTER TABLE `plant_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `plant`
--
ALTER TABLE `plant`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT a táblához `plant_type`
--
ALTER TABLE `plant_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Megkötések a táblához `order_plant`
--
ALTER TABLE `order_plant`
  ADD CONSTRAINT `fk_order_plant` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_plant_order` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Megkötések a táblához `plant`
--
ALTER TABLE `plant`
  ADD CONSTRAINT `fk_plant_type` FOREIGN KEY (`type_id`) REFERENCES `plant_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
