CREATE TABLE IF NOT EXISTS `user` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password CHAR(64) NOT NULL,
    `group` ENUM('vásárló','dolgozó','vezető') NOT NULL,
    address varchar(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX email_UNIQUE (email ASC)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `plant_type` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    description TEXT,
    unit ENUM('darab','tő','szál') NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX name_UNIQUE (name ASC)
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `plant` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    type_id INT UNSIGNED NOT NULL,
    property TEXT,
    requirement TEXT,
    plantable_from VARCHAR(10),
    plantable_to VARCHAR(10),
    quantity INT UNSIGNED NOT NULL,
    price DECIMAL(10,2),
    PRIMARY KEY (id),
    UNIQUE INDEX name_UNIQUE (name ASC),
    INDEX fk_plant_type_index (type_id ASC),
    CONSTRAINT fk_plant_type
        FOREIGN KEY (type_id)
        REFERENCES `plant_type` (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `order` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    user_id INT UNSIGNED NOT NULL,
    date INT UNSIGNED NOT NULL,
    payment ENUM('készpénz','átutalás','utánvét') NOT NULL,
    delivery ENUM('futárszolgálat','postai kézbesítés','személyes átvétel') NOT NULL,
    comment MEDIUMTEXT NOT NULL,
    PRIMARY KEY (id),
    INDEX fk_order_user_index (user_id ASC),
    CONSTRAINT fk_order_user
        FOREIGN KEY (user_id)
        REFERENCES `user` (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `order_plant` (
    order_id INT UNSIGNED NOT NULL,
    plant_id INT UNSIGNED NOT NULL,
    quantity INT UNSIGNED NOT NULL,
    PRIMARY KEY (order_id, plant_id),
    INDEX fk_order_plant_index (order_id ASC),
    INDEX fk_plant_order_index (plant_id ASC),
    CONSTRAINT fk_order_plant
        FOREIGN KEY (order_id)
        REFERENCES `order` (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION,
    CONSTRAINT fk_plant_order
        FOREIGN KEY (plant_id)
        REFERENCES plant (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO `user` (`id`, `name`, `email`, `password`, `group`, `address`) VALUES
(NULL, 'Admin', 'info@nurserygarden.com', 'Admin', 'vezető', '1234 Bivajbasznád Tököli út 6');