-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2015. Dec 20. 15:28
-- Kiszolgáló verziója: 10.0.17-MariaDB
-- PHP verzió: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `nurserygarden`
--

--
-- A tábla adatainak kiíratása `order`
--

INSERT INTO `order` (`id`, `user_id`, `date`, `payment`, `delivery`, `comment`) VALUES
(2, 4, 1450471136, 'készpénz', 'futárszolgálat', ''),
(3, 4, 1450471150, 'átutalás', 'személyes átvétel', ''),
(4, 4, 1450608549, 'készpénz', 'személyes átvétel', '');

--
-- A tábla adatainak kiíratása `order_plant`
--

INSERT INTO `order_plant` (`order_id`, `plant_id`, `quantity`) VALUES
(4, 1, 2),
(4, 2, 1);

--
-- A tábla adatainak kiíratása `plant`
--

INSERT INTO `plant` (`id`, `name`, `type_id`, `property`, `requirement`, `plantable_from`, `plantable_to`, `quantity`, `price`) VALUES
(1, 'teszt', 1, 'jellemzők', 'igények', '08.20', '10.30', 50, '2000.00'),
(2, 'eeee', 2, 'easdasd', 'asdasda', '10.02', '12.11', 30, '123546.00');

--
-- A tábla adatainak kiíratása `plant_type`
--

INSERT INTO `plant_type` (`id`, `name`, `description`, `unit`) VALUES
(1, 'Fák', '', 'darab'),
(2, 'teszt', '', 'darab');

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `group`, `address`) VALUES
(1, 'Admin', 'info@nurserygarden.com', 'Admin', 'vezető', '1234 Bivajbasznád Tököli út 6'),
(2, 'test', 'test@test.hu', 'test', 'vásárló', 'test'),
(3, 'test2', 'asd@asd.hu', 'test2', 'dolgozó', 'test2'),
(4, 'Pisi Misi', 'pisi@misi.hu', 'misi', 'vásárló', 'istenhátamögött');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
