<?php
/* @var $this PlantTypeController */
/* @var $model PlantType */

$this->breadcrumbs=array(
	'Plant Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PlantType', 'url'=>array('index')),
	array('label'=>'Create PlantType', 'url'=>array('create')),
	array('label'=>'View PlantType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PlantType', 'url'=>array('admin')),
);
?>

<h1>Update PlantType <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>