<?php
/* @var $this PlantTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Növénytípusok',
);
?>

<h1>Növénytípusok</h1>

<?php
echo CHtml::link('Új hozzáadása', array('planttype/create'));
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'description',
        'unit',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
