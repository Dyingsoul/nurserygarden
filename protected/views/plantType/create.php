<?php
/* @var $this PlantTypeController */
/* @var $model PlantType */

$this->breadcrumbs=array(
	'Plant Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PlantType', 'url'=>array('index')),
	array('label'=>'Manage PlantType', 'url'=>array('admin')),
);
?>

<h1>Create PlantType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>