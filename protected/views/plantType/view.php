<?php
/* @var $this PlantTypeController */
/* @var $model PlantType */

$this->breadcrumbs=array(
	'Plant Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PlantType', 'url'=>array('index')),
	array('label'=>'Create PlantType', 'url'=>array('create')),
	array('label'=>'Update PlantType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PlantType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PlantType', 'url'=>array('admin')),
);
?>

<h1>View PlantType #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'unit',
	),
)); ?>
