<?php
/* @var $this PlantController */
/* @var $model Plant */

$this->breadcrumbs=array(
	'Plants'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Plant', 'url'=>array('index')),
	array('label'=>'Create Plant', 'url'=>array('create')),
	array('label'=>'Update Plant', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Plant', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Plant', 'url'=>array('admin')),
);
?>

<h1>View Plant #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'type_id',
		'property',
		'requirement',
		'plantable_from',
		'plantable_to',
		'quantity',
		'price',
	),
)); ?>
