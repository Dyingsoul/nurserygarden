<?php
/* @var $this PlantController */
/* @var $data Plant */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
    <?php echo CHtml::encode($data->name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('type_id')); ?>:</b>
    <?php echo CHtml::encode($data->type->name); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('property')); ?>:</b>
    <?php echo CHtml::encode($data->property); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('requirement')); ?>:</b>
    <?php echo CHtml::encode($data->requirement); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('plantable_from')); ?>:</b>
    <?php echo CHtml::encode($data->plantable_from); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('plantable_to')); ?>:</b>
    <?php echo CHtml::encode($data->plantable_to); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
    <?php echo CHtml::encode($data->quantity); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
    <?php echo CHtml::encode($data->price); ?>
    <br />
    
    <?php
    echo CHtml::link('Kosárba',array('plant/addtocart','id'=>$data->id));
    ?>



</div>