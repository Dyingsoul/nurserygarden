<?php
/* @var $this PlantController */
/* @var $model Plant */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'plant-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type_id'); ?>
        <?php
        $list = CHtml::listData(PlantType::model()->findAll(), 'id', 'name');
        echo $form->dropDownList($model, 'type_id', $list);
        ?>
        <?php echo $form->error($model, 'type_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'property'); ?>
        <?php echo $form->textArea($model, 'property', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'property'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'requirement'); ?>
        <?php echo $form->textArea($model, 'requirement', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'requirement'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'plantable_from'); ?>
        <?php echo $form->textField($model, 'plantable_from', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'plantable_from'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'plantable_to'); ?>
        <?php echo $form->textField($model, 'plantable_to', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'plantable_to'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'quantity'); ?>
        <?php echo $form->textField($model, 'quantity', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'quantity'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'price'); ?>
        <?php echo $form->textField($model, 'price', array('size' => 10, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'price'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->