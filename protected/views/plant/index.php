<?php
/* @var $this PlantController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Növények',
);
?>

<h1>Növények</h1>

<?php
echo CHtml::link('Új hozzáadása', array('plant/create'));
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        array(
            'name' => 'type_id',
            'value' => '$data->type->name',
        ),
        'property',
        'requirement',
        'plantable_from',
        'plantable_to',
        'quantity',
        'price',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
