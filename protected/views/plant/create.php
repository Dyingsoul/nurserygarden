<?php
/* @var $this PlantController */
/* @var $model Plant */

$this->breadcrumbs=array(
	'Plants'=>array('index'),
	'Hozzáadás',
);
?>

<h1>Növény hozzáadása</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>