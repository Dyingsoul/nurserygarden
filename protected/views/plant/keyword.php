<?php
/* @var $this PlantController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Növények',
);
?>

<h1><?php echo $keyword; ?> találatok</h1>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
));
?>
