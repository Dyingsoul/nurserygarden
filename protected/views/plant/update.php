<?php
/* @var $this PlantController */
/* @var $model Plant */

$this->breadcrumbs=array(
	'Plants'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Plant', 'url'=>array('index')),
	array('label'=>'Create Plant', 'url'=>array('create')),
	array('label'=>'View Plant', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Plant', 'url'=>array('admin')),
);
?>

<h1>Update Plant <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>