<?php
/* @var $this PlantController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Növények',
);
?>

<h1>Növények</h1>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'menu-dropdown-form',
        'enableAjaxValidation' => true,
    ));

    echo CHtml::dropDownList('planttype_id', $selected_category_id, // The selected category set on controller action
            $list, // the dropdown options set on controller action
            array('prompt' => 'Select Category',)
    );

    $this->endWidget();
    ?>
</div>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
    'id' => 'items_list' //for filtering
));


Yii::app()->clientScript->registerScript('items_update', "$('#planttype_id').change(function(){
        $.fn.yiiListView.update('items_list', {
                data: $(this).serialize(),
            }
        );
    });
    return false;", CClientScript::POS_READY);
?>
