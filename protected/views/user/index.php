<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Felhasználók',
);
?>

<h1>Felhasználók</h1>

<?php
echo CHtml::link('Új hozzáadása', array('user/create'));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'email',
        'group',
        'address',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
