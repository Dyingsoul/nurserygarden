<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Felhasználók'=>array('index'),
	'Hozzáadás',
);
?>

<h1>Felhasználó létrehozás</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>