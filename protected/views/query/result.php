<?php
/* @var $this QueriesController */
/* @var $model Query */
/* @var $rows Rows */

$this->breadcrumbs = array(
    'Lekérdezések' => array("/query"),
    'Eredmények'
);
?>
<h1>Lekérdezések</h1>
<?php
if (count($rows) > 0) {
    switch ($model->type) {
        case "orders":
            ?>
            <table class="table table-bordered">
                <tr>
                    <th>Időtartam</th>
                    <th>Eladott növények száma</th>
                </tr>
                <tr>
                    <td><?php echo $rows['name']; ?></td>
                    <td><?php echo $rows['sum']; ?></td>
                </tr>
            </table>
            <?php
            break;

        case "mostorderedplants":
            ?>
            <table class="table table-bordered">
                <tr>
                    <th>Növény neve</th>
                    <th>Eladott mennyiség</th>
                </tr>
                <tr>
                    <td><?php echo $rows['name']; ?></td>
                    <td><?php echo $rows['sum']; ?></td>
                </tr>
            </table>
            <?php
            break;
        case "leastorderedplants":
            ?>
            <table class="table table-bordered">
                <tr>
                    <th>Növény neve</th>
                    <th>Eladott mennyiség</th>
                </tr>
                <tr>
                    <td><?php echo $rows['name']; ?></td>
                    <td><?php echo $rows['sum']; ?></td>
                </tr>
            </table>
            <?php
            break;
    }
} else {
    ?>
    <h2 class="alert alert-danger">Nem található a lekérdezésnek megfelelő adat!</h2>
    <?php
}
?>