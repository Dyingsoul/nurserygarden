<?php
/* @var $this QueriesController */
/* @var $model Query */

$this->breadcrumbs = array(
    'Lekérdezések',
);

$queryTypes = CHtml::listData(Query::getFixtures('types'), 'type', 'value');
?>
<h1>Lekérdezések</h1>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'queries-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('query/result'),
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'fromDate'); ?>
        <?php echo $form->dateField($model, 'fromDate'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'toDate'); ?>
        <?php echo $form->dateField($model, 'toDate'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php
        echo $form->dropDownList($model, 'type', $queryTypes);
        ?>
    </div>

    <div class="row">
        <?php echo CHtml::submitButton('Lekérdezés'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>