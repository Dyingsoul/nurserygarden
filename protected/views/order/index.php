<?php
/* @var $this OrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Rendelések',
);
?>

<h1>Rendelések</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'name' => 'user_id',
            'value' => '$data->user->name',
        ),
        array(
            'name' => 'date',
            'value' => 'date("Y-m-d H:i:s",$data->date)',
        ),
        'payment',
        'delivery',
        'comment',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
