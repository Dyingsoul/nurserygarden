<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'order-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'payment'); ?>
        <?php
        $list = array('készpénz' => 'készpénz', 'átutalás' => 'átutalás', 'utánvét' => 'utánvét');
        echo $form->dropDownList($model, 'payment', $list);
        ?>
        <?php echo $form->error($model, 'payment'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'delivery'); ?>
        <?php
        $list = array('futárszolgálat' => 'futárszolgálat', 'postai kézbesítés' => 'postai kézbesítés', 'személyes átvétel' => 'személyes átvétel');
        echo $form->dropDownList($model, 'delivery', $list);
        ?>
        <?php echo $form->error($model, 'delivery'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'comment'); ?>
        <?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comment'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->