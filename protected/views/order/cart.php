<?php

/* @var $this SiteController */

if (isset($_SESSION['cart'])) {
    echo "<div>Növény neve - mennyiség</div>";
    foreach ($_SESSION['cart'] as $key => $value){
        echo "<div>$key - $value </div>";
    }
    
    echo CHtml::link('Kosár ürítése',array('order/emptycart'))."<br>";
    
    echo CHtml::link('Tovább a rendeléshez',array('order/create'));
} else {
    echo "Nincs termék a kosárban";
}
