<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="hu">

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <div id="mainmenu">
                <div style="float: left">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label' => 'Főoldal', 'url' => array('/plant/shop'), 'visible' => !(isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető")))),
                            array('label' => 'Kosár', 'url' => array('/order/cart'), 'visible' => !(isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető")))),
                            array('label' => 'Kontakt', 'url' => array('/site/contact'), 'visible' => !(isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető")))),
                            array('label' => 'Növények', 'url' => array('/plant/index'), 'visible' => isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető"))),
                            array('label' => 'Növénytípusok', 'url' => array('/plantType/index'), 'visible' => isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető"))),
                            array('label' => 'Rendelések', 'url' => array('/order/index'), 'visible' => isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető"))),
                            array('label' => 'Felhasználók', 'url' => array('/user/index'), 'visible' => isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető"))),
                            array('label' => 'Lekérdezések', 'url' => array('/query/index'), 'visible' => isset(Yii::app()->user->group) && ((Yii::app()->user->group === "dolgozó") || (Yii::app()->user->group === "vezető"))),
                            array('label' => 'Bejelentkezés', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                            array('label' => 'Kijelentkezés', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                        ),
                    ));
                    ?>
                </div>
                <div style="float:right; padding: 2px">
                    <?php echo CHtml::form(Yii::app()->createUrl('plant/searchbykeyword'), 'get') ?>
                    <?php echo CHtml::textField('q', 'search') ?>
                    <?php echo CHtml::endForm() ?>
                </div>
                <div style="clear:both"></div>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> by RFT team.<br/>
                All Rights Reserved.<br/>
                <?php echo Yii::powered(); ?>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
