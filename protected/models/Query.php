<?php

class Query extends CFormModel {

    public $type;
    public $fromDate;
    public $toDate;

    public function rules() {
        return array(
            array('type, fromDate, toDate', 'required'),
        );
    }

    public function attributeLabels() {
        return array(
            'type' => 'Lekérdezés típusa',
            'fromDate' => 'Dátumtól',
            'toDate' => 'Dátumig',
        );
    }

    public static function getFixtures($name = null) {
        $fixtures = array(
            'types' => array(
                array(
                    "type" => "orders",
                    "value" => "Rendelések"
                ),
                array(
                    "type" => "mostorderedplants",
                    "value" => "Legtöbbet eladott növény",
                ),
                array(
                    "type" => "leastorderedplants",
                    "value" => "Legkevesebbet eladott növény"
                ),
            )
        );
        return empty($name) ? $fixtures : $fixtures[$name];
    }

}
