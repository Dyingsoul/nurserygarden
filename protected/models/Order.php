<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property string $id
 * @property string $user_id
 * @property string $date
 * @property string $payment
 * @property string $delivery
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Plant[] $plants
 */
class Order extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'order';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('payment, delivery', 'required'),
            array('user_id, date, payment', 'length', 'max' => 10),
            array('delivery', 'length', 'max' => 20),
            array('date', 'default', 'value' => time()),
            array('payment', 'in', 'range' => array('készpénz', 'átutalás', 'utánvét')),
            array('delivery', 'in', 'range' => array('futárszolgálat', 'postai kézbesítés', 'személyes átvétel')),
            // The following rule is used by search().
            array('id, user_id, date, payment, delivery, comment', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'plants' => array(self::MANY_MANY, 'Plant', 'order_plant(order_id, plant_id)'),
            'orderplants' => array(self::HAS_MANY, 'OrderPlant', 'order_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'Felhasználó',
            'date' => 'Dátum',
            'payment' => 'Fizetési mód',
            'delivery' => 'Szállítási mód',
            'comment' => 'Megjegyzés',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('payment', $this->payment, true);
        $criteria->compare('delivery', $this->delivery, true);
        $criteria->compare('comment', $this->comment, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
