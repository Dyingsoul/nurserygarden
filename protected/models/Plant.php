<?php

/**
 * This is the model class for table "plant".
 *
 * The followings are the available columns in table 'plant':
 * @property string $id
 * @property string $name
 * @property string $type_id
 * @property string $property
 * @property string $requirement
 * @property string $plantable_from
 * @property string $plantable_to
 * @property string $quantity
 * @property string $price
 *
 * The followings are the available model relations:
 * @property Order[] $orders
 * @property PlantType $type
 */
class Plant extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'plant';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, type_id, property, requirement, plantable_from, plantable_to, quantity, price', 'required'),
            array('name', 'length', 'max' => 50),
            array('type_id, plantable_from, plantable_to, quantity, price', 'length', 'max' => 10),
            array('property, requirement', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, type_id, property, requirement, plantable_from, plantable_to, quantity, price', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'orders' => array(self::MANY_MANY, 'Order', 'order_plant(plant_id, order_id)'),
            'type' => array(self::BELONGS_TO, 'PlantType', 'type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Név',
            'type_id' => 'Típus',
            'property' => 'Jellemzők',
            'requirement' => 'Igények',
            'plantable_from' => 'Ültethető (tól)',
            'plantable_to' => 'Ültethető (ig)',
            'quantity' => 'Raktáron',
            'price' => 'Ár',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type_id', $this->type_id, true);
        $criteria->compare('property', $this->property, true);
        $criteria->compare('requirement', $this->requirement, true);
        $criteria->compare('plantable_from', $this->plantable_from, true);
        $criteria->compare('plantable_to', $this->plantable_to, true);
        $criteria->compare('quantity', $this->quantity, true);
        $criteria->compare('price', $this->price, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Plant the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
