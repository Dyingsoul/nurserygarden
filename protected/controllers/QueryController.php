<?php

class QueryController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'result'),
                'users' => array('@'),
                'expression' => 'isset(Yii::app()->user->group) &&
                    ((Yii::app()->user->group==="dolgozó") ||
                    (Yii::app()->user->group==="vezető"))',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $model = new Query;
        $model->unsetAttributes();
        $model->fromDate = date("Y-m-d", strtotime("-10 days"));
        $model->toDate = date("Y-m-d");

        $this->render('index', array("model" => $model));
    }

    public function actionResult() {
        $model = new Query;
        $model->unsetAttributes();

        if (isset($_POST['Query'])) {
            $rows = array();
            $values = filter_input(INPUT_POST, 'Query', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $model->attributes = $values;

            if ($model->validate()) {
                switch ($model->type) {
                    case "orders":
                        $orders = Order::model()->with('plants')->findAll('date BETWEEN :from AND :to', array(':from' => strtotime($model->fromDate), ':to' => (strtotime($model->toDate) + 3600 * 24)));
                        if ($orders != null && count($orders) > 0) {
                            $sum = 0;
                            foreach ($orders as $order) {
                                foreach ($order->orderplants as $orderplant) {
                                    $sum += $orderplant->quantity;
                                }
                            }
                            $rows = array(
                                'name' => $model->fromDate . " - " . $model->toDate,
                                'sum' => $sum
                            );
                        }
                        break;
                    case "mostorderedplants":
                        $orderplants = OrderPlant::model()->with('order', 'plant')->findall('order.date BETWEEN :from AND :to', array(':from' => strtotime($model->fromDate), ':to' => (strtotime($model->toDate) + 3600 * 24)));
                        $plants = array();
                        foreach ($orderplants as $orderplant) {
                            if (isset($plants[$orderplant->plant->name])) {
                                $plants[$orderplant->plant->name] += $orderplant->quantity;
                            } else {
                                $plants[$orderplant->plant->name] = $orderplant->quantity;
                            }
                        }
                        arsort($plants);
                        $value = current($plants);
                        $key = key($plants);
                        $rows = array(
                            'name' => $key,
                            'sum' => $value
                        );
                        break;
                    case "leastorderedplants":
                        $orderplants = OrderPlant::model()->with('order', 'plant')->findall('order.date BETWEEN :from AND :to', array(':from' => strtotime($model->fromDate), ':to' => (strtotime($model->toDate) + 3600 * 24)));
                        $plants = array();
                        foreach ($orderplants as $orderplant) {
                            if (isset($plants[$orderplant->plant->name])) {
                                $plants[$orderplant->plant->name] += $orderplant->quantity;
                            } else {
                                $plants[$orderplant->plant->name] = $orderplant->quantity;
                            }
                        }
                        arsort($plants);
                        $value = end($plants);
                        $key = key($plants);
                        $rows = array(
                            'name' => $key,
                            'sum' => $value
                        );
                        break;
                }

                $this->render('result', array('model' => $model, 'rows' => $rows));
            } else {
                $this->redirect(array("index"));
            }
        } else {
            $this->redirect(array("index"));
        }
    }

}
