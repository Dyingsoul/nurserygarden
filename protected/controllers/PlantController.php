<?php

class PlantController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('shop', 'shopitem', 'addtocart', 'searchbykeyword'),
                'users' => array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete'),
                'users' => array('@'),
                'expression' => 'isset(Yii::app()->user->group) &&
                    ((Yii::app()->user->group==="dolgozó") ||
                    (Yii::app()->user->group==="vezető"))',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Plant;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plant'])) {
            $model->attributes = $_POST['Plant'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plant'])) {
            $model->attributes = $_POST['Plant'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $model = new Plant('search');
        $model->unsetAttributes();  // clear any default values
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionShop() {
        $criteria = (isset($_GET['planttype_id']) && $_GET['planttype_id'] != "") ?
                array(
            'condition' => 'type_id=' . $_GET['planttype_id'],
            'order' => 'id ASC',
                ) : array(
            'order' => 'id ASC',
        );
        $options = array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        );
        $dataProvider = new CActiveDataProvider('Plant', $options);
        // retrieve categories from db for the dropdown list
        $models = PlantType::model()->findAll(array('order' => 'name'));
        // format categories as $key=>$value with listData
        $list = CHtml::listData($models, 'id', 'name');
        $selected_category_id = (isset($_GET['planttype_id'])) ? $_GET['planttype_id'] : "";

        $this->render('shop', array(
            'dataProvider' => $dataProvider,
            'list' => $list,
            'selected_category_id' => $selected_category_id,
        ));
    }

    public function actionAddtocart($id) {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }

        $plant = Plant::model()->findByPk($id);
        if (isset($_SESSION['cart'][$plant->name])) {
            $_SESSION['cart'][$plant->name] ++;
        } else {
            $_SESSION['cart'][$plant->name] = 1;
        }
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('shop'));
    }

    public function actionSearchByKeyword($q) {
        //based on the shop method's type filtering
        $criteria = (isset($_GET['q']) && $_GET['q'] != "") ?
                array(
            'condition' => "name LIKE '%" . $_GET['q']."%' OR property LIKE '%" . $_GET['q']."%' OR requirement LIKE '%" . $_GET['q']."%'",
            'order' => 'id ASC',
                ) : array(
            'order' => 'id ASC',
        );
        $options = array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20,
            ),
        );
        $dataProvider = new CActiveDataProvider('Plant', $options);
        $keyword = (isset($_GET['q'])) ?  CHtml::encode($_GET['q']) : "";

        $this->render('keyword', array(
            'dataProvider' => $dataProvider,
            'keyword' => $keyword,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Plant the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Plant::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Plant $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'plant-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
